/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Control;

import Modelo.AnalizadorSintactico;
import Vista.Consola;

/**
 *
 * @author Lab05pcdocente
 */
public class Cadena {

    public static void main(String[] args) {
        Consola pantalla = new Consola();
        String palabra = pantalla.leerCadena("Digite una cadena:");
        System.out.println(palabra);
        AnalizadorSintactico analiza = new AnalizadorSintactico(palabra);
        pantalla.imprimir(analiza.toString());
        pantalla.imprimir(analiza.toString());
        if (analiza.isPalindrome()) {
            pantalla.imprimir(palabra + ", Es palíndrome");
        } else {
            pantalla.imprimir(palabra + ", NO Es palíndrome");
        }
    }

}
