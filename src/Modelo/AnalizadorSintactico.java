/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author Lab05pcdocente
 */
public class AnalizadorSintactico {

    Stack<Character> pila = new Stack();
    Queue<Character> cola = new LinkedList<>();

    public AnalizadorSintactico() {
    }

    public AnalizadorSintactico(String cadena) {
        this.crearCola_pila(cadena);
    }

    private void crearCola_pila(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.charAt(i);
            this.pila.push(caracter);
            this.cola.offer(caracter);

        }
    }

    public boolean isPalindrome() {
        String cadena = "";
        while (!this.pila.empty()) {
            char elemento = this.pila.pop();
            char elemento2 = this.cola.poll();
            if (elemento != elemento2) {
                return false;
            }
            cadena += elemento2;
        }
        //vuelve y crea
        this.crearCola_pila(cadena);
        return true;
    }

    /**
     * Comprueba si la cadena tiene paréntesis balanceados
     * @return true si está balanceado, o false en caso contrario
     */
    public boolean isBalanceadoParentesis()
    {
        return false;
    }
    
    /**
     * Comprueba si la cadena contiene el doble de a(s) que de b(s)
     * Ejemplo:
     * Cadena=aabbaa --> true
     * Cadena=aabbaabaa--> true
     * Siempre usando PILAS Y COLAS
     * @return true si cumple la condición o false en caso contrario
     */
    public boolean isDoble()
    {
        return false;
    }
    
    @Override
    public String toString() {
        String msg = "";
        String msg1 = "Pila:\t";
        String msg2 = "Cola:\t";
        String cadena = "";
        while (!this.pila.empty()) {
            char elemento = this.pila.pop();
            char elemento2 = this.cola.poll();
            msg1 += elemento + "\t";
            msg2 += elemento2 + "\t";
            cadena += elemento2;
        }
        msg = msg1 + "\n" + msg2;
        //vuelve y crea
        this.crearCola_pila(cadena);
        return msg;
    }
}
